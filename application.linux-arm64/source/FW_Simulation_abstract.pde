abstract class Simulation {
  // array of entities in the simulation
  Entity[] entity;
  // Array of walls for the window edges
  Wall[] wall;
  // Array of cells
  Entity[][] cells;
  // Size of cells
  int cellSize = 5;
  // Array of neighbour cells
  Boolean[][][] neighbourCells;
  
  Simulation(int max_entities) {
    this.entity = new Entity[max_entities];
    this.cells = new Entity[max_entities][max_entities];
    this.neighbourCells = new Boolean[4][max_entities][max_entities];
  }
  
  /* Set entity to the simulation */
  void setEntity(Entity entity, int index) {
    this.entity[index] = entity;
  }
  
  void bounce() {
    
  }
  
  void explode() {
    
  }
  
  /* Borders arround the window */
  void setupWalls() {
    this.wall = new Wall[4];
    //top
    this.wall[0] = new Wall(width + 200, 1, width / 2 - 100, 0);
    this.wall[0].setColor(#0000ff);
    this.wall[0].name = "up";
    this.wall[0].alive = true;
    //bottom
    this.wall[1] = new Wall(width + 200, 1, width / 2  - 100, height);
    this.wall[1].setColor(#0000ff);
    this.wall[1].name = "down";
    this.wall[1].alive = true;
    //left
    this.wall[2] = new Wall(1, height + 200, 0, height / 2 - 100);
    this.wall[2].setColor(#0000ff);
    this.wall[2].name = "left";
    this.wall[2].alive = true;
    //right
    this.wall[3] = new Wall(1, height + 200, width, height / 2 - 100);
    this.wall[3].setColor(#0000ff);
    this.wall[3].name = "right";
    this.wall[3].alive = true;
    println("walls set");
  }
  
  /* Add wall to a corner */
  void createWall(String name) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name)) {
        this.wall[i].alive = true;
      }
    }
  }
  
  /* Add 2 walls to corners (used when connecting or disconnection to host) */
  void createWall(String name1, String name2) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2)) {
        this.wall[i].alive = true;
      }
    }
  }
  
  /* Add 3 walls to corners (used when connecting or disconnection to host) */
  void createWall(String name1, String name2, String name3) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2) || this.wall[i].name.equals(name3)) {
        this.wall[i].alive = true;
      }
    }
  }
  
  /* Add 4 walls to corners (used when connecting or disconnection to host) */
  void createWall(String name1, String name2, String name3, String name4) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2) || this.wall[i].name.equals(name3) || this.wall[i].name.equals(name4)) {
        this.wall[i].alive = true;
      }
    }
  }
  
  /* Delete wall from a corner */
  void deleteWall(String name) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name)) {
        this.wall[i].alive = false;
      }
    }
  }
  
  /* Delete 2 walls from corners (used when connecting or disconnection to host) */
  void deleteWall(String name1, String name2) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2)) {
        this.wall[i].alive = false;
      }
    }
  }
  
  /* Delete 3 walls from corners (used when connecting or disconnection to host) */
  void deleteWall(String name1, String name2, String name3) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2)) {
        this.wall[i].alive = false;
      }
    }
  }
  
  /* Delete 4 walls from corners (used when connecting or disconnection to host) */
  void deleteWall(String name1, String name2, String name3, String name4) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2) || this.wall[i].name.equals(name3) || this.wall[i].name.equals(name4)) {
        this.wall[i].alive = false;
      }
    }
  }
  
  /* set entity to window (used when entity travels between devices) */
  void updateEntity(String name, boolean alive, int xspd, int yspd, int xsize, int ysize, String direction, String current_device, int xpos, int ypos){
    for(int i = 0; i < this.entity.length; i++) {
      if (this.entity[i] != null && this.entity[i].name.equals(name)) {
        this.entity[i].alive = alive;
        this.entity[i].xpos = xpos;
        this.entity[i].ypos = ypos;
        this.entity[i].xspd = xspd;
        this.entity[i].yspd = yspd;
        this.entity[i].xsize = xsize;
        this.entity[i].ysize = ysize;
        this.entity[i].direction = direction;
        this.entity[i].current_device = current_device;
      }
    }
  }
  
  void checkCell(String direction, String cells, String current_device) {
    if (this.neighbourCells != null) {
      String[] splited_cells = cells.split("\\|");
      int x_length = this.neighbourCells[0].length,
          y_length = this.neighbourCells[0][0].length;
      
      for (int x = 0; x < x_length; x++) {
        String[] cell_cols = splited_cells[x].split(",");
        for (int y = 0; y < y_length; y++) {
          if (direction.equals("top")) {
            this.neighbourCells[0][x][y] = Boolean.valueOf(cell_cols[y]);
          }
          else if (direction.equals("down")) {
            this.neighbourCells[1][x][y] = Boolean.valueOf(cell_cols[y]);
          }
          else if (direction.equals("left")) {
            this.neighbourCells[2][x][y] = Boolean.valueOf(cell_cols[y]);
          }
          else if (direction.equals("right")) {
            this.neighbourCells[3][x][y] = Boolean.valueOf(cell_cols[y]);
          }
        }
      }
    }
  }
  
  void draw() {
    background(255);
    if(this.entity[0] != null && this.entity[0].isAlive()) {
      this.entity[0].draw();
    }
  }
  
  void update() {
    this.entity[0].update();
    this.entity[0].screenOut();
  }

  void keyPressed() {}

  void mousePressed() {}

  void mouseDragged() {}
}
