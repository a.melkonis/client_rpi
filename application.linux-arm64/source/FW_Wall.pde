class Wall extends Entity {
  color fill;
  
  Wall(int _width, int _height, int x, int y) {
    this.xsize = _width;
    this.ysize = _height;
    this.xpos = x;
    this.ypos = y;
  }
  
  void setColor(color fill) {
    this.fill = fill;
  }
  
  void update() {

  }
  
  void draw() {
    if (this.isAlive()) {
      rectMode(CENTER);
      fill(this.fill);
      noStroke();
      rect(this.xpos, this.ypos, this.xsize, this.ysize);
    }
  }

}
