class Cell extends Entity {
  XML xml;
  float gravity;
  float bounce;
  
  Cell(int x,int y, int size, String current_device) {
    this.name = "Cell";
    this.current_device = current_device;
    xml = loadXML("config_cell.xml");
    if (size <= 0) {
      this.xsize = int(xml.getChild("cell_size").getContent());
    } else {
      this.xsize = size;
    }
    this.ysize = xsize;
    
    this.xpos = x;
    this.ypos = y;
  }
  
  void update() {
    
  }
  
  void draw() {
    if (this.isAlive()) {
      fill(0);
      rect(xpos, ypos, xsize, ysize);
    }
  }

}
