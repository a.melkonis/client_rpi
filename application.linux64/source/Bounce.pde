class Bounce extends Simulation {
  
  Bounce(int max_entities, Entity[] entities) {
    super(max_entities);
    for (int i = 0; i < entities.length; i++) {
      this.setEntity(entities[i], i);
      this.entity[i].direction = "all";
      this.entity[i].alive = false;
    }
    
    this.setupWalls();
  }
  
  void draw() {
    background(255);
    for (int i = 0; i < this.wall.length; i++) {
      if (this.wall[i] != null && this.wall[i].isAlive())
        this.wall[i].draw();
    }
    for (int i = 0; i < this.entity.length; i++) {
      if (this.entity[i] != null && this.entity[i].isAlive()) {
        this.entity[i].draw();
      }
    }
  }
  
  void update() {
    for (int i = 0; i < this.entity.length; i++) {
      if (this.entity[i] != null && this.entity[i].isAlive()) {
        this.entity[i].update();
        for (int j = 0; j < this.wall.length; j++) {
          if (this.wall[j] != null) {
            this.entity[i].bounce(this.wall[j]);
          }
        }
        
        if (i < this.entity.length - 1 && this.entity[i+1] != null)
          this.entity[i].bounce(this.entity[i+1]);
        
        
        this.entity[i].screenOut();
      }
    }
  }

  void keyPressed() {
    for (int i = 0; i < this.entity.length; i++) {
      if (this.entity[i] != null && this.entity[i].isAlive()) {
        if (keyCode == UP) {
          this.entity[i].direction = "up";
          if (this.entity[i].yspd > 0)
            this.entity[i].yspd *= -1;
        } else if (keyCode == DOWN) {
          this.entity[i].direction = "down";
          if (this.entity[i].yspd < 0)
            this.entity[i].yspd *= -1;
        } else if (keyCode == LEFT) {
          this.entity[i].direction = "left";
          if (this.entity[i].xspd > 0)
            this.entity[i].xspd *= -1;
        } else if (keyCode == RIGHT) {
          this.entity[i].direction = "right";
          if (this.entity[i].xspd < 0)
            this.entity[i].xspd *= -1;
        }
        
        this.entity[i].update();
      }
    }
  }

  void mousePressed() {
    if (mouseButton == RIGHT) {
      for (int i = 0; i < this.entity.length; i++) {
        if (this.entity[i] != null) {
            this.entity[i].direction = "all";
            this.entity[i].alive = true;
            this.entity[i].xpos = 50;
            this.entity[i].ypos = 50;
            if (device != null) {
              this.entity[i].current_device = device.server.toString();
            }
          
          this.entity[i].update();
        }
      }
    }
  }

  void mouseDragged() {
    boolean can_move;
    for (int i = 0; i < this.entity.length; i++) {
      can_move = true;
      if (this.entity[i] != null && this.entity[i].isAlive()) {
        this.entity[i].update();
        for (int j = 0; j < this.wall.length; j++) {
          if (this.wall[j] != null) {
            if (!this.entity[i].canMove(this.wall[j])) {
              can_move = false;
            }
          }
        }
        
        if (i < this.entity.length -1 && this.entity[i+1] != null) {
          if (!this.entity[i].canMove(this.entity[i+1])) {
            can_move = false;
          }
        }
        
        if (can_move)
        this.entity[i].mouseDragged();
      }
    }
  }
  
}
