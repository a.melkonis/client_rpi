/*
Responsible for every entity and simulation on the screen.
*/

class Area {
  Entity[] entity;
  Simulation simulation;
  int[] screen;
  int[] position;
  XML config;
  
  
  Area() {
    screen = new int[2];
    position = new int[2];
    config = loadXML("config.xml");
    screen[0] = int(config.getChild("screen_width").getContent());
    screen[1] = int(config.getChild("screen_height").getContent());
    position[0] = int(config.getChild("screen_position_x").getContent());
    position[1] = int(config.getChild("screen_position_y").getContent());
    entity = new Entity[int(config.getChild("max_entities").getContent())];
  }
  
  /* Add new entity to the area */
  void setEntity(Entity entity, int index) {
      this.entity[index] = entity;
  }
  
  /* Get entity from the area */
  Entity getEntity(int index) {
      return this.entity[index];
  }
  
  /* Add simulation to the area */
  void setSimulation(Simulation simulation){
    this.simulation = simulation;
  }
  
}
