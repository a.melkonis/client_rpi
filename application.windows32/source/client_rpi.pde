import processing.net.*;
import java.util.Iterator;
import java.util.Set;
import java.util.Arrays;
import java.lang.reflect.Method;

Device device;
DeviceArea area;

XML config;
int[] screen = new int[2];
int max_entities = 0;

void settings() {
  config = loadXML("config.xml");
  
  device = new Device(this, int(config.getChild("screen_width").getContent()), int(config.getChild("screen_height").getContent()));
  device.initClient(config.getChild("server").getContent(), int(config.getChild("port").getContent()));
  
  max_entities = int(config.getChild("max_entities").getContent());
  area = new DeviceArea(device.client);
  size(area.screen[0], area.screen[1]);
}

void setup(){
  if (area.position[0] >= 0 && area.position[1] >= 0) {
    surface.setLocation(area.position[0], area.position[1]);
  }

  if (config.getChild("simulation") != null) {
    /* Setup Simulation */
    if (config.getChild("simulation").getContent().equals("ball")) {
      // Ball
      for (int i = 0; i < max_entities; i++) {
        area.setEntity(new Ball((i * i) + 35, (i * i) + 35, device.client.toString()), i);
        area.entity[i].setName("ball_" + i);
      }

      // Bounce Ball
      area.setSimulation(new Bounce(max_entities, area.entity));
    }
    else if (config.getChild("simulation").getContent().equals("cell")) {
      // Cell
      int max_cell_entities = (width/max_entities * height/max_entities);

      area.entity = new Entity[max_cell_entities];
      for (int i = 0; i < max_cell_entities; i++) {
        area.setEntity(new Cell((i * i) + 35, (i * i) + 35, 0/*max_entities*/, device.client.toString()), i);
        area.entity[i].setName("cell_" + i);
      }

      // Life Cell
      area.setSimulation(new Life(max_cell_entities, area.entity));
    }
  }
}

void draw(){
  area.client = device.ensureConnection();
  area.simulation.update();
  area.simulation.draw();

  area.doServerAction(device);
  area.informNeighbours();
  for(int i = 0; i < area.simulation.entity.length; i++) {
    if (area.simulation.entity[i] != null && area.entityIsTransfering(area.simulation.entity[i], device.client)) {
    
    }
  }
}

void clientEvent(Client client) {
  JSONObject json;
  String data;
  data = client.readString();

  if (data != null) {
    try {
      device.json = parseJSONObject(data);    
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
}

void keyPressed() {
  area.simulation.keyPressed();
}

void mousePressed() {
  area.simulation.mousePressed();
}

void mouseDragged() {
  area.simulation.mouseDragged();
}
