abstract class Entity {
  int xpos, ypos;
  int xspd, yspd;
  int xsize, ysize, xradius, yradius;
  String name;
  String direction;
  String current_device;
  boolean alive;
  
  float mousex, mousey;
  
  boolean isAlive(){
    return this.alive;
  }
  
  void setName(String name) {
    this.name = name;
  }
  
  void move(String direction) {
    if (direction.contains("left") || direction.contains("right") || direction.contains("all"))
      xpos += xspd;//xdir;
    if (direction.contains("up") || direction.contains("down") || direction.contains("all"))
      ypos += yspd;//ydir;
  }
  
  void reverse(String direction) {
    if (direction.contains("left") || direction.contains("right") || direction.contains("all"))
      xspd *= -1;
    if (direction.contains("up") || direction.contains("down") || direction.contains("all"))
      yspd *= -1;
  }
  
  void bounce(Entity entity) {
    if (this.isAlive() && entity.isAlive()) {      
      // X
      if (this.xpos + this.xsize / 2 + this.xspd > entity.xpos - entity.xsize / 2
        && this.xpos - this.xsize / 2 + this.xspd < entity.xpos + entity.xsize / 2
        && this.ypos + this.ysize / 2 > entity.ypos - entity.ysize / 2
        && this.ypos < entity.ypos + entity.ysize / 2) {
        this.xspd *= -1;
        if (this.direction.contains("right"))
          this.direction.replace("right", "left");
        else if(this.direction.contains("left"))
          this.direction.replace("left", "right");
      }
      
      // Y
      if (this.xpos + this.xsize / 2  > entity.xpos - entity.xsize / 2
        && this.xpos < entity.xpos + entity.xsize / 2
        && this.ypos + this.ysize / 2 + this.yspd > entity.ypos - entity.ysize / 2
        && this.ypos - this.ysize / 2 + this.yspd < entity.ypos + entity.ysize / 2) {
          this.yspd *= -1;
          if (this.direction.contains("down"))
            this.direction.replace("down", "up");
          else if(this.direction.contains("up"))
            this.direction.replace("up", "down");
      }
    }
  }
  
  boolean canMove(Entity entity) {
    if (this.xpos + this.xsize / 2 + this.xspd > entity.xpos - entity.xsize / 2
        && this.xpos - this.xsize / 2 + this.xspd < entity.xpos + entity.xsize / 2
        && this.ypos + this.ysize / 2 > entity.ypos - entity.ysize / 2
        && this.ypos < entity.ypos + entity.ysize / 2) {
          return false;
      }
      
      // Y
      if (this.xpos + this.xsize / 2  > entity.xpos - entity.xsize / 2
        && this.xpos < entity.xpos + entity.xsize / 2
        && this.ypos + this.ysize / 2 + this.yspd > entity.ypos - entity.ysize / 2
        && this.ypos - this.ysize / 2 + this.yspd < entity.ypos + entity.ysize / 2) {
          return false;
      }
    
    return true;
  }
  
  void bounce(int x, int y) {
    if (xpos > x - xsize || xpos < x + xsize) {
      xspd *= -1;
    }
    if (ypos > y - ysize || ypos < y + ysize) {
      yspd *= -1;
    }
  }
  
  void bounce() {
    if (xpos > width - xsize / 2 || xpos < xsize / 2) {
      xspd *= -1;
    }
    if (ypos > height - ysize / 2 || ypos < ysize / 2) {
      yspd *= -1;
    }
  }
  
  void screenOut() {
    if (this.xpos > width + this.xsize || this.xpos < 0 - this.xsize
      || this.ypos > height + this.ysize || this.ypos < 0 - this.ysize) {
        this.alive = false;
      }
  }
  
  void top() {

  }

  void bottom() {
    
  }

  void left() {
    
  }
  
  void right(){
    
  }
  
  void angle() {
    
  }
  
  void update() {
    
  }
  
  void draw() {
    
  }
  
  void mouseDragged() {
    if (this.mousex > this.xpos && this.xpos < 0){
      this.xspd *= -1;
    }
    else if (this.mousex < this.xpos && this.xpos > 0) {
      this.xspd *= -1;
    }
    
    if (this.mousey > ypos && this.ypos < 0 )
      this.yspd *= -1;
    else if (this.mousey < this.ypos && this.ypos > 0) {
      this.yspd *= -1;
    }
    
    this.mousex = this.xpos;
    this.mousey = this.ypos;
    this.xpos = mouseX;
    this.ypos = mouseY;
  }

}
