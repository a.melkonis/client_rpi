class Ball extends Entity {
  XML xml;
  float gravity;
  float bounce;
  
  Ball(int x,int y, String current_device) {
    this.name = "Ball";
    this.current_device = current_device;
    xml = loadXML("config_ball.xml");
    this.xsize = int(xml.getChild("ball_size").getContent());
    this.ysize = xsize;
    this.xradius = xsize / 2;
    this.yradius = ysize / 2;
    
    this.xpos = x;
    this.ypos = y;
    this.xspd = int(xml.getChild("speed").getContent());
    this.yspd = int(xml.getChild("speed").getContent());
    
    this.gravity = 0.98;
    this.bounce = -1;
  }
  
  void bounceOld() {
    if (xpos > width - xsize / 2 || xpos < xsize / 2) {
      xspd *= -1;
    }
    
    if (ypos > height - ysize / 2 || ypos < ysize / 2) {
      yspd *= -1;
    }
  }
  
  void update() {
    move(direction);
  }
  
  void draw() {
    if (this.isAlive()) {
      fill(0);
      ellipse(xpos, ypos, xsize, ysize);
    }
  }

}
