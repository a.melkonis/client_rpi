/*
Responsible for managing device and connections between devices.
*/

class Device {
  int screen[];
  PApplet p;
  String ip_address;
  int port;
  Server server;
  Client client;
  JSONObject json;
  String last_client;
  boolean started;
  
  
  Device(PApplet p, int width, int height) {
    screen = new int[] {width, height};
    this.p = p;
  }
  
  /* Start Server host */
  void initServer(int port) {
    try {
      this.server = new Server(p, port);
    }
    catch(Exception e) {
        println(e.getCause());
    }
  }
  
  /* Start connection with the Server */
  Client initClient(String ip_address, int port) {
    this.ip_address = ip_address;
    this.port = port;
    try {
      this.client = new Client(this.p, this.ip_address, this.port);
      
      /* for globars, unfinished */
      JSONAction message = new JSONAction().setClass("Device")
      .addMethod("setResolution")
      .addArg("setResolution", "name", "String", this.client.toString())
      .addArg("setResolution", "device_width", "int", Integer.toString(this.screen[0]))
      .addArg("setResolution", "device_height", "int", Integer.toString(this.screen[1]));
      
      
      this.client.write(message.getJSON());
    }
    catch(Exception e) {
        println(e.getCause());
    }
    finally {
      return this.client;
    }
  }
  
  /* Close server host */
  void stopServer() {
    this.server.stop();
  }
  
  /* Close connection with the host */
  void stopClient() {
    this.client.stop();
  }

  /* Close and restart connection withe the host */
  Client ensureConnection() {
    try {
      if (! this.started && this.client != null) {
        this.client.stop();
        this.client = this.initClient(this.ip_address, this.port);
        this.started = true;
      }
    }
    catch(Exception e) {
        println(e.getCause());
    }
    finally {
      return this.client;
    }
  }
  
}
