class Life extends Simulation {
  // Size of cells
  int cellSize = 5;

  // How likely for a cell to be alive at start (in percentage)
  float probabilityOfAliveAtStart = 15;
  
  // Variables for timer
  int interval = 100;
  int lastRecordedTime = 0;
  
  // Colors for active/inactive cells
  color alive = color(0, 200, 0);
  color dead = color(0);

  // Buffer to record the state of the cells and use this while changing the others in the interations
  Entity[][] cellsBuffer; 
  
  Life(int max_entities, Entity[] entities) {
    super(max_entities);
    for (int i = 0; i < entities.length; i++) {
      this.setEntity(entities[i], i);
      this.entity[i].alive = false;
    }
    
    this.cellSize = this.entity[0].xsize;    
    this.cells = new Entity[width / this.cellSize][height / this.cellSize];
    this.cellsBuffer = new Entity[width / this.cellSize][height / this.cellSize];
    this.neighbourCells = new Boolean[4][width / this.cellSize][height / this.cellSize];
    
    // This stroke will draw the background grid
    stroke(48);
    
    // Initialization of cells
    int i = 0;
    for (int x = 0; x < width / this.cellSize; x++) {
      for (int y = 0; y < height / this.cellSize; y++) {
        float state = random(100);
        if (state > this.probabilityOfAliveAtStart) { 
          state = 0;
        }
        else {
          state = 1;
        }
        cells[x][y] = this.entity[i];
        cellsBuffer[x][y] = new Cell(this.entity[i].xpos, this.entity[i].ypos, this.entity[i].xsize, this.entity[i].current_device);
        cells[x][y].alive = int(state) > 0 ? true : false; // Save state of each cell
        i++;
      }
    }
    background(0); // Fill in black in case cells don't cover all the windows
  }
  
  void iteration() { // When the clock ticks
    // Save cells to buffer (so we opeate with one array keeping the other intact)
    for (int x = 0; x < width / this.cellSize; x++) {
      for (int y = 0; y < height / this.cellSize; y++) {
        cellsBuffer[x][y].alive = cells[x][y].alive;
      }
    }
  
    // Visit each cell:
    for (int x = 0; x < width / this.cellSize; x++) {
      for (int y = 0; y < height / this.cellSize; y++) {
        // And visit all the neighbours of each cell
        int neighbours = 0; // We'll count the neighbours
        for (int xx = x-1; xx <= x+1; xx++) {
          for (int yy = y-1; yy <= y+1; yy++) {

            // TOP Neighbour
            if (xx >= 0 && xx < width / this.cellSize && yy == 0) {
              try {
                Boolean toCheck = neighbourCells[0][xx][(height / this.cellSize)-1];
                if (toCheck != null && toCheck) {
                  neighbours ++;
                }
              }
              catch(Exception e) {
                println("exception: " + e.getCause());
              }
            }

            // BOTTOM Neighbour
            if (xx >= 0 && xx < width / this.cellSize && yy == height / this.cellSize) {
              try {
                Boolean toCheck = neighbourCells[0][xx][0];
                if (toCheck != null && toCheck) {
                  neighbours ++;
                }
              }
              catch(Exception e) {
                println("exception: " + e.getCause());
              }
            }

            // LEFT Neighbour
            if (xx == 0 && yy > 0 && yy < height / this.cellSize) {
              try {
                Boolean toCheck = neighbourCells[2][(width / this.cellSize)-1][yy];
                if (toCheck != null && toCheck) {
                  neighbours ++;
                }
              }
              catch(Exception e) {
                println("exception: " + e.getCause());
              }
            }

            // RIGHT Neighbour
            if (xx == width / this.cellSize && yy >= 0 && yy < (height / this.cellSize)) {
              try {
                Boolean toCheck = neighbourCells[3][0][yy];
                if (toCheck != null && toCheck) {
                  neighbours ++;
                }
              }
              catch(Exception e) {
                println("exception: " + e.getCause());
              }
            }

            if (((xx >= 0) && (xx < width / this.cellSize)) && ((yy >=0 ) && (yy < height / this.cellSize))) { // Make sure you are not out of bounds
              if (!((xx == x) && (yy == y))) { // Make sure to to check against self
                if (cellsBuffer[xx][yy].alive == true){
                  neighbours ++; // Check alive neighbours and count them
                }
              } // End of if
            } // End of if
          } // End of yy loop
        } //End of xx loop
        // We've checked the neigbours: apply rules!
        if (cellsBuffer[x][y].alive == true) { // The cell is alive: kill it if necessary
          if (neighbours < 2 || neighbours > 3) {
            cells[x][y].alive = false; // Die unless it has 2 or 3 neighbours
          }
        } 
        else { // The cell is dead: make it live if necessary      
          if (neighbours == 3 ) {
            cells[x][y].alive = true; // Only if it has 3 neighbours
          }
        } // End of if
      } // End of y loop
    } // End of x loop
  } // End of function
  
  
  void draw() {
    for (int x = 0; x < width / this.cellSize; x++) {
      for (int y = 0; y < height / this.cellSize; y++) {
        if (cells[x][y].alive == true) {
          fill(alive); // If alive
        }
        else {
          fill(dead); // If dead
        }
        rect (x * this.cellSize, y * this.cellSize, this.cellSize, this.cellSize);
      }
    }
  }


  // /* debug neigbours */
  // void draw() {
  //   for (int x = 0; x < width / this.cellSize; x++) {
  //     for (int y = 0; y < height / this.cellSize; y++) {
  //       if (this.neighbours[x][y] != null) {
  //         if (neighbours[x][y] == true) {
  //           fill(alive); // If alive
  //         }
  //         else {
  //           fill(dead); // If dead
  //         }
  //       }
  //       else {
  //           fill(dead); // If dead
  //         }
  //       rect (x * this.cellSize, y * this.cellSize, this.cellSize, this.cellSize);
  //     }
  //   }
  // }
  
  void update() {
    // Iterate if timer ticks
    if (millis() - this.lastRecordedTime>interval) {
      this.iteration();
      this.lastRecordedTime = millis();
      this.neighbourCells = new Boolean[4][width / this.cellSize][height / this.cellSize];
    }
  }

  void keyPressed() {
    if (key=='r' || key == 'R') {
      // Restart: reinitialization of cells
      for (int x = 0; x < width / this.cellSize; x++) {
        for (int y = 0; y < height / this.cellSize; y++) {
          float state = random(100);
          if (state > probabilityOfAliveAtStart) {
            state = 0;
          }
          else {
            state = 1;
          }
          this.cells[x][y].alive = int(state) > 0 ? true : false;
        }
      }
    }
  }
  
}
