/*
Usage: Add target class name as main class target.
Add action methods for the class object to run.
Set arguments for every method attached to the JSON object. Each method must have correct and complete arguments.
*/

class JSONAction {
  private JSONObject json;
  public String classname;
  
  /* Initialize json object  */
  JSONAction JSONAction() {
    this.json = new JSONObject();
    
    return this;
  }
  
  /* Get JSON as string */
  String getJSON() {
    return this.json.toString();
  }
  
  /* Get JSON Object */
  JSONObject getJSONObject() {
    return this.json;
  }
  
  /* Clear JSON Object */
  void reset() {
    this.json = new JSONObject();
    this.classname = "";
  }
  
  /* Set target classname to run actions */
  JSONAction setClass(String classname) {
    this.reset();
    this.json.setJSONObject(classname, new JSONObject());
    this.classname = classname;
    
    return this;
  }
  
  /* If target classname has actions to run */
  boolean hasMethods() {
    return this.json.isNull("methods");
  }
  
  /* If set action methods have arguments included */
  boolean hasArgs(String method) {
    JSONObject methods = this.json.getJSONObject(this.classname).getJSONObject("methods");
    
    for(int i = 0; i < methods.size(); i++) {
      if (methods.getJSONObject(Integer.toString(i)).getString("method").equals(method)) {
        return ! methods.getJSONObject(Integer.toString(i)).isNull("args");
      }
    }
    
    return false;
  }
  
  /* Set action methods for the target classname */
  JSONAction addMethod(String method) {
    JSONObject methods = new JSONObject();
    if (this.json.getJSONObject(this.classname).isNull("methods")) {
      this.json.getJSONObject(this.classname).setJSONObject("methods", new JSONObject());
    }
    
    methods = this.json.getJSONObject(this.classname).getJSONObject("methods");
    int count = 0;
    if (! methods.isNull("0")) {
      count = methods.size();
    }

    
      methods.setJSONObject(Integer.toString(count), new JSONObject()
        .setString("method", method)
    );
    
    return this;
  }
  
  /* Add arguments to the methods */
  JSONAction addArg(String method_name, String name, String type, String value) {
    JSONObject methods = this.json.getJSONObject(this.classname).getJSONObject("methods");
    JSONObject method = new JSONObject();
    int i;
    for(i = 0; i < methods.size(); i++) {
      if (methods.getJSONObject(Integer.toString(i)).getString("method").equals(method_name)) {
        method = methods.getJSONObject(Integer.toString(i));
        break;
      }
    }
    
    if (method.isNull("args")) {
      method.setJSONObject("args", new JSONObject());
    }
      
    method = method.getJSONObject("args");
    int count = 0;
    if (! method.isNull("0")) {
      count = method.size();
    }
    
    method.setJSONObject(Integer.toString(count), new JSONObject()
      .setString("name", name)
      .setString("type", type)
      .setString("value", value)
    );
    
    return this;
  }
}
