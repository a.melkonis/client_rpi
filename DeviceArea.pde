class DeviceArea extends Area {
  Client client;
  XML config;
  
  DeviceArea(Client client) {
    this.client = client;
    config = loadXML("config.xml");
  }
  
  boolean entityIsTransfering(Entity entity, Client client) {
    if (entity.current_device.equals(this.client.toString()) && entity.isAlive() && (entity.xpos > width - entity.xsize / 2 || entity.xpos < entity.xsize / 2 || entity.ypos > height - entity.ysize / 2 || entity.ypos < entity.ysize / 2)) {
      int index = 0;
      
      JSONAction message = new JSONAction().setClass("Entity").addMethod("update")
      .addArg("update", "name", "String", entity.name)
      .addArg("update", "alive", "Boolean", "true")
      .addArg("update", "xspd", "int", Integer.toString(entity.xspd))
      .addArg("update", "yspd", "int", Integer.toString(entity.yspd))
      .addArg("update", "xsize", "int", Integer.toString(entity.xsize))
      .addArg("update", "ysize", "int", Integer.toString(entity.ysize))
      .addArg("update", "direction", "String", entity.direction)
      .addArg("update", "current_device", "String", entity.current_device);
        
      if (entity.ypos < entity.ysize / 2 && entity.yspd < 0) {
        message.addArg("update", "xpos", "int", Integer.toString(entity.xpos))
        .addArg("update", "ypos", "int", Integer.toString(entity.ypos))
        .addArg("update", "device_direction", "String", "up");

        this.client.write(message.getJSON());
        return true;
      }
      
      if (entity.ypos > height - entity.ysize / 2 && entity.yspd > 0) {
        message.addArg("update", "xpos", "int", Integer.toString(entity.xpos))
        .addArg("update", "ypos", "int", Integer.toString(entity.ypos - height))
        .addArg("update", "device_direction", "String", "down");

        this.client.write(message.getJSON());
        return true;
      }
      
      if (entity.xpos < entity.xsize / 2 && entity.xspd < 0) {
        message.addArg("update", "xpos", "int", Integer.toString(entity.xpos))
        .addArg("update", "ypos", "int", Integer.toString(entity.ypos))
        .addArg("update", "device_direction", "String", "left");

        this.client.write(message.getJSON());
        return true;
      }
      
      if (entity.xpos > width - entity.xsize / 2 && entity.xspd > 0) {
        message.addArg("update", "xpos", "int", Integer.toString(entity.xpos - width))
        .addArg("update", "ypos", "int", Integer.toString(entity.ypos))
        .addArg("update", "device_direction", "String", "right");

        this.client.write(message.getJSON());
        return true;
      }


    }
    return false;
  }
  
  void doServerAction(Device device) {
    if (device.json == null)
      return;

    Set Classes = device.json.keys();
    for(Object classname : Classes) {
      JSONObject methods = device.json.getJSONObject(classname.toString()).getJSONObject("methods");
      Set method_indexes = methods.keys();
      
      for (Object method_index : method_indexes) {
        JSONObject method = methods.getJSONObject(method_index.toString()); //<>//
        JSONObject args = method.getJSONObject("args");
        Class[] params = new Class[args.size()];
        Object[] values = new Object[args.size()];
        
        for (int i = 0; i < args.size(); i++) {
          JSONObject argument = args.getJSONObject(String.valueOf(i));
          
          switch(argument.getString("type")) {
            case "String":
              params[i] = String.class;
              values[i] = argument.getString("value");
              break;
            case "int": case "Integer":
              params[i] = int.class;
              values[i] = argument.getInt("value");
              break;
            case "float": case "Float":
              params[i] = float.class;
              values[i] = argument.getFloat("value");
              break;
            case "double": case "Double":
              params[i] = double.class;
              values[i] = argument.getDouble("value");
              break;
            case "boolean": case "Boolean":
              params[i] = boolean.class;
              values[i] = argument.getBoolean("value");
              break;
            case "char": case "Character":
              params[i] = char.class;
              values[i] = argument.getString("value");
              break;
          }
        }
        
        if (method.getString("method").equals("update")) {
          values = this.repositionEntity(values);
          values[7] = this.client.toString();
          values = this.removeTheElement(values, 10);
          params = this.removeTheElement(params, 10); 
        }
        
        try {
          Method m = this.simulation.getClass().getMethod(method.getString("method") + classname.toString(), params);
          m.invoke(this.simulation, values);
        }
        catch(Exception e) {
          e.printStackTrace();
        }
      }
    }

    device.json = null;
  }


  boolean informNeighbours() {
    if (this.simulation.cells == null || this.simulation.cells[0][0] == null) {
      return false;
    }

    boolean sent = false;
    
    int x_length = this.simulation.cells.length,
        y_length = this.simulation.cells[0].length;
        
    String[][] top = new String[2][x_length], 
              bottom = new String[2][x_length],
              left = new String[2][y_length], 
              right = new String[2][y_length];

    String[] stringColumns = new String[x_length];

    for (int x = 0; x < x_length; x++) {
      stringColumns[x] = "";
      for (int y = 0; y < y_length; y++) {
        if (this.simulation.cells[x][y] == null) continue;

        stringColumns[x] += Boolean.toString(this.simulation.cells[x][y].alive);
        if (y < y_length-1) stringColumns[x] += ",";
        
      }

    }
    
    int index = 0;
    String mergedArray = String.join("|", stringColumns);
    
    JSONAction message = new JSONAction();
    message.setClass("Cell").addMethod("check")
      .addArg("check", "direction", "String", "bottom")
      .addArg("check", "cells", "String", mergedArray)
      .addArg("check", "current_device", "String", this.client.toString());
    this.client.write(message.getJSON());
    sent = true;

    message.reset();
    message.setClass("Cell").addMethod("check")
      .addArg("check", "direction", "String", "top")
      .addArg("check", "cells", "String", mergedArray)
      .addArg("check", "current_device", "String", this.client.toString());
    this.client.write(message.getJSON());
    sent = true;
  
    message.reset();
    message.setClass("Cell").addMethod("check")
      .addArg("check", "direction", "String", "right")
      .addArg("check", "cells", "String", mergedArray)
      .addArg("check", "current_device", "String", this.client.toString());
    this.client.write(message.getJSON());
    sent = true;
  
    message.reset();
    message.setClass("Cell").addMethod("check")
      .addArg("check", "direction", "String", "left")
      .addArg("check", "cells", "String", mergedArray)
      .addArg("check", "current_device", "String", this.client.toString());
    this.client.write(message.getJSON());
    sent = true;
    
    return sent;
  }

  
  Object[] repositionEntity(Object[] values) {
    Entity[] entity = this.simulation.entity;
    for (int i = 0; i < entity.length; i++) {
      
      //if entity name equals value name
      if (entity[i] != null && entity[i].name.toString().equals(values[0].toString())) {
        
        //if X position SMALLER than X size divided by 2 (entity radius) AND X speed LESS than 0 (going left) 
        if (Integer.valueOf(values[8].toString()) < Integer.valueOf(values[4].toString()) / 2 && Integer.valueOf(values[2].toString()) < 0) {
          values[8] = Integer.valueOf(values[8].toString()) + width;
        }
        
        //if X position GREATER than window width - X size divided by 2 (entity radius) AND X speed GREATER than 0 (going right) 
        else if (Integer.valueOf(values[8].toString()) > width - Integer.valueOf(values[4].toString()) / 2 && Integer.valueOf(values[2].toString()) > 0) {
          values[8] = Integer.valueOf(values[8].toString()) - width;
        }
        
        //if Y position SMALLER than Y size divided by 2 (entity radius) AND Y speed LESS than 0 (going up) 
        else if (Integer.valueOf(values[9].toString()) < Integer.valueOf(values[5].toString()) / 2 && Integer.valueOf(values[3].toString()) < 0) {
          values[9] = Integer.valueOf(values[9].toString()) + height;
        }
        
        //if Y position GREATER than window height - Y size divided by 2 (entity radius) AND Y speed GREATER than 0 (going down) 
        else if (Integer.valueOf(values[9].toString()) > height - Integer.valueOf(values[5].toString()) / 2 && Integer.valueOf(values[3].toString()) > 0) {
          values[9] = Integer.valueOf(values[9].toString()) - height;
        }
      }
    }
    
    return values;
  }
  
  Object[] removeTheElement(Object[] arr, int index) { 
      // If the array is empty
      // or the index is not in array range
      // return the original array
      if (arr == null
          || index < 0
          || index >= arr.length) {

          return arr; 
      }

      // Create another array of size one less 
      Object[] anotherArray = new Object[arr.length - 1]; 

      // Copy the elements except the index 
      // from original array to the other array 
      for (int i = 0, k = 0; i < arr.length; i++) { 

          // if the index is 
          // the removal element index 
          if (i == index) { 
              continue; 
          } 

          // if the index is not 
          // the removal element index 
          anotherArray[k++] = arr[i]; 
      } 

      // return the resultant array 
      return anotherArray; 
  }
  
  Class[] removeTheElement(Class[] arr, int index) { 
      // If the array is empty
      // or the index is not in array range
      // return the original array
      if (arr == null
          || index < 0
          || index >= arr.length) {

          return arr; 
      } 

      // Create another array of size one less 
      Class[] anotherArray = new Class[arr.length - 1]; 

      // Copy the elements except the index 
      // from original array to the other array 
      for (int i = 0, k = 0; i < arr.length; i++) { 

          // if the index is 
          // the removal element index 
          if (i == index) { 
              continue; 
          } 

          // if the index is not 
          // the removal element index 
          anotherArray[k++] = arr[i]; 
      } 

      // return the resultant array 
      return anotherArray; 
  }
  
  void debug() {
    println(this.simulation.entity[0].name);
    println(this.simulation.entity[0].alive);
    println(this.simulation.entity[0].xpos);
    println(this.simulation.entity[0].ypos);
    println(this.simulation.entity[0].xspd);
    println(this.simulation.entity[0].yspd);
    println(this.simulation.entity[0].xsize);
    println(this.simulation.entity[0].ysize);
    println(this.simulation.entity[0].direction);
  }
  
}
